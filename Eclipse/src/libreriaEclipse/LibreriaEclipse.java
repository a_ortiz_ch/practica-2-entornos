package libreriaEclipse;
import java.util.Scanner;

public class LibreriaEclipse {

	public static void opcion1 (Scanner lector) {
		
		System.out.println("Dime tu altura en m");
		double altura = lector.nextDouble();
		
		System.out.println("Ahora escribe tu peso en kg");
		double peso = lector.nextDouble();
		
		double imcVariable = (peso / (altura * altura));
		
		if (imcVariable < 16) {
			
			System.out.println("Criterio de ingreso en hospital");
			
		} else if (imcVariable >= 16 && imcVariable < 17) {
			
			System.out.println("Infrapeso");
			
		} else if (imcVariable >= 17 && imcVariable < 18) {
			
			System.out.println("Bajo peso");
			
		} else if (imcVariable >= 18 && imcVariable < 25) {
			
			System.out.println("Peso normal (saludable)");
			
		} else if (imcVariable >= 25 && imcVariable < 30) {
			
			System.out.println("Sobrepeso (obesidad de grado 1)");
			
		} else if (imcVariable >= 30 && imcVariable < 35) {
			
			System.out.println("Sobrepeso cr�nico (obesidad de grado 2)");
			
		} else if (imcVariable >= 35 && imcVariable < 40) {
			
			System.out.println("Obesidad prem�rbida (obesidad de grado 3)");
			
		} else {
			
			System.out.println("Obesidad m�rbida (obesidad de grado 4)");
			
		}
		
	}
	
	public static void opcion2 () {
		
		
		
	}
	
	public static void opcion3 () {
		
		
		
	}
	
	public static void opcion4 () {
		
		
		
	}
	
	public static void main (String [] args) {
		
		Scanner lector = new Scanner (System.in);
		
		int opcion;
		
		do {
			
			System.out.println("Elige una opci�n");
			System.out.println("Opci�n 1: Calcular la masa corporal de una persona");
			System.out.println("Opci�n 2: ");
			System.out.println("Opci�n 3: ");
			System.out.println("Opci�n 4: ");
			System.out.println("Opci�n 5: Salir");
			opcion = lector.nextInt();
			
			switch (opcion) {
			
				case 1:
					
					opcion1 (lector);
					break;
					
				case 2:
					
					opcion2 ();
					break;
					
				case 3:
					
					opcion3 ();
					break;
					
				case 4:
					
					opcion4 ();
					break;
			
			}
			
			
		} while (opcion != 5);
		
		lector.close();
		
	}
	
}
