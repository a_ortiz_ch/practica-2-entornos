package ventanaEclipse;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class VentanaEclipse extends JFrame {

	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtContrasea;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaEclipse frame = new VentanaEclipse();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaEclipse() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 604, 424);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnLogin = new JMenu("Archivo");
		menuBar.add(mnLogin);
		
		JMenu mnNuevo = new JMenu("Nuevo");
		mnLogin.add(mnNuevo);
		
		JMenuItem mntmProyecto = new JMenuItem("Proyecto");
		mnNuevo.add(mntmProyecto);
		
		JMenuItem mntmVentana = new JMenuItem("Ventana");
		mnNuevo.add(mntmVentana);
		
		JMenuItem mntmAbrirProyecto = new JMenuItem("Abrir proyecto");
		mnLogin.add(mntmAbrirProyecto);
		
		JMenuItem mntmGuerdar = new JMenuItem("Guardar");
		mnLogin.add(mntmGuerdar);
		
		JMenu mnPginaPrincipal = new JMenu("Editar");
		menuBar.add(mnPginaPrincipal);
		
		JMenuItem mntmDeshacer = new JMenuItem("Deshacer");
		mnPginaPrincipal.add(mntmDeshacer);
		
		JMenuItem mntmCopiar = new JMenuItem("Copiar");
		mnPginaPrincipal.add(mntmCopiar);
		
		JMenuItem mntmPegar = new JMenuItem("Pegar");
		mnPginaPrincipal.add(mntmPegar);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 153, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtNombre = new JTextField();
		txtNombre.setBackground(new Color(204, 153, 255));
		txtNombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtNombre.setText("Nombre: ");
		txtNombre.setBounds(10, 29, 96, 19);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(116, 29, 118, 19);
		contentPane.add(textPane);
		
		txtContrasea = new JTextField();
		txtContrasea.setBackground(new Color(204, 153, 255));
		txtContrasea.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtContrasea.setText("Contrase\u00F1a:");
		txtContrasea.setBounds(10, 58, 96, 19);
		contentPane.add(txtContrasea);
		txtContrasea.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(116, 58, 118, 19);
		contentPane.add(passwordField);
		
		JCheckBox chckbxHeLedoY = new JCheckBox("He le\u00EDdo y acepto los t\u00E9rminos de privacidad");
		chckbxHeLedoY.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		chckbxHeLedoY.setBackground(new Color(204, 153, 255));
		chckbxHeLedoY.setBounds(10, 103, 492, 21);
		contentPane.add(chckbxHeLedoY);
		
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnEnviar.setBounds(10, 284, 85, 21);
		contentPane.add(btnEnviar);
		
		JTextPane txtpnEscribaAquLos = new JTextPane();
		txtpnEscribaAquLos.setFont(new Font("Comic Sans MS", Font.PLAIN, 14));
		txtpnEscribaAquLos.setText("Escriba aqu\u00ED los comentarios:");
		txtpnEscribaAquLos.setBounds(10, 145, 479, 129);
		contentPane.add(txtpnEscribaAquLos);
	}
}